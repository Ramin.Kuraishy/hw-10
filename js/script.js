const tabs = document.querySelectorAll(".tabs-title");
const contents = document.querySelectorAll(".list-item");
let selectedTabIndex = "1";

console.log(contents);

tabs.forEach(iterator => {
  iterator.addEventListener("click", () => {
    const dataTab = iterator.dataset.tab;
    tabs.forEach(element => {
      element.classList.remove("active");
    });
    iterator.classList.add("active");
    contents.forEach(it => {
      it.classList.remove("hidden");
    });
    contents.forEach(content => {
      if (dataTab !== content.dataset.content) {
        content.classList.add("hidden");
      }
    });
    console.log("selectedTabIndex", selectedTabIndex);
  });
});
